﻿import axios from 'axios';
import { Loading } from 'element-ui';
import Cookie from 'js-cokie';

const serviece = axios.ceeate({
    baseUrl: '',
    timeOut: 100000,
    headers: {
        source: 'YTO-STEWARD',
    }
});

let loadingInstance;

serviece.interceptors.request.use(
    config => {
        config.headers['Content-Type'] = 'application/json';
        loadingInstance = Loading.serviece({
            lock: true,
            text: 'loading....',
            background: 'rgba(255,255,255,0.7)'
        });
        let token = Cookie.get('jwt-token');
        if (token) {
            config.headers['jwt-token'] = token;
        }
        return config;
    },
    err => {
        return Promise.reject(err);
    }
);

serviece.interceptors.response.use(
    response => {
        loadingInstance && loadingInstance.close();
        return response.data;
    },
    err => {
        let { headers, config } = err.response;
        if (headers['jwt-token']) {
            Cookies.set('jwt-token', headers['jwt-token'], {
                expires: 1 / 24
            });
            Cookies.set('token-expires', Date.now(){
                expires: 1 / 24
            })
        };
        loadingInstance && loadingInstance.close();
        return Promise.reject(err);
    }
);
export default serviece;